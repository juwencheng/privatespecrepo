#
# Be sure to run `pod lib lint JWCCategory.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'JWCCategory'
  s.version          = '0.1.0'
  s.summary          = '常用到的类别集合，可以分别使用。'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://juwencheng@bitbucket.org/juwencheng/jwccategory'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Juwencheng' => 'juwenz@icloud.com' }
  s.source           = { :git => 'https://juwencheng@bitbucket.org/juwencheng/jwccategory.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

#s.source_files = 'JWCCategory/Classes/**/*'
    s.subspec 'DateUtilities' do |dateUtilities|
        dateUtilities.source_files = 'JWCCategory/Classes/NSDate/**'
        dateUtilities.public_header_files = 'JWCCategory/Classes/NSDate/*.h'
    end
    s.subspec 'FontUtilities' do |fontUtilities|
        fontUtilities.source_files = 'JWCCategory/Classes/UIFont/**'
        fontUtilities.public_header_files = 'JWCCategory/Classes/UIFont/*.h'
    end
    s.subspec 'StringUtilities' do |stringUtilities|
        stringUtilities.source_files = 'JWCCategory/Classes/StringUtilities/**'
        stringUtilities.public_header_files = 'JWCCategory/Classes/StringUtilities/*.h'
    end
  
  # s.resource_bundles = {
  #   'JWCCategory' => ['JWCCategory/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
