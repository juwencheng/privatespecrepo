#
# Be sure to run `pod lib lint XibView.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'XibView'
  s.version          = '0.1.0'
  s.summary          = '快速关联UIView和Xib，只需集成此类即可'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  XibView可以快速的将UIView和Xib进行关联，支持frame和autolayout两种模式。对于习惯与xib撰写界面的朋友，是最佳的选择。
                       DESC

  s.homepage         = 'https://juwencheng@bitbucket.org/juwencheng/xibview'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'juwencheng' => 'juwenz@icloud.com' }
  s.source           = { :git => 'https://juwencheng@bitbucket.org/juwencheng/xibview.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'XibView/Classes/**/*'
  
  # s.resource_bundles = {
  #   'XibView' => ['XibView/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
