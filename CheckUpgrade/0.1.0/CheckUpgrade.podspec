#
# Be sure to run `pod lib lint CheckUpgrade.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CheckUpgrade'
  s.version          = '0.1.0'
  s.summary          = '检测版本更新工具'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
提供检测更新功能，每次打开软件后，检测是否有版本更新，如果有，弹出提示框，并且让用户选择是否立即更新，如果需要不需要立即更新，可以选择1天，3天后再次提醒。
                       DESC

  s.homepage         = 'https://juwencheng@bitbucket.org/juwencheng/checkupgrade'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Juwencheng' => 'juwenz@icloud.com' }
  s.source           = { :git => 'https://juwencheng@bitbucket.org/juwencheng/checkupgrade.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'CheckUpgrade/Classes/**/*'
  
  # s.resource_bundles = {
  #   'CheckUpgrade' => ['CheckUpgrade/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
